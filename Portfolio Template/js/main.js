/* eslint-env es6 */
/* eslint-disable no-console */

$('.hamburger-menu').click(function(event){
	event.stopPropagation();
	if ($('main').hasClass('active')){
        setTimeout( function(){
            $('.mobile-nav').css('display','none');
        },200);
		$('main').removeClass('active');
	} else {
        $('.mobile-nav').css('display','block');
		$('main').addClass('active');
	}
    
});

const cards = document.querySelectorAll('.experience-blocks');

for (let i = 0; i < cards.length; i++) {
    const card = cards[i];
    card.addEventListener('mousemove', startRotate);
    card.addEventListener('mouseout', stopRotate);
}


function startRotate(event) {
    const cardItem = this.querySelector('.experience-block');
    const halfHeight = cardItem.offsetHeight / 2;
    const halfWidth = cardItem.offsetWidth / 2;
    
    cardItem.style.transform = 'rotateX(' + - (event.offsetY - halfHeight) / 7 + 'deg) rotateY(' + (event.offsetX - halfWidth) / 7 + 'deg)';
}

function stopRotate(event) {
    const cardItem = this.querySelector('.experience-block');
    cardItem.style.transform = 'rotate(0)';
}

$(function() {
    $("[data-scroll]").on("click", function(event){
        
        setTimeout( function(){
            $('.mobile-nav').css('display','none');
        },200);
		$('main').removeClass('active');        
        event.preventDefault();
        
        let elementId = $(this).data("scroll");
        let elementOffset = $(elementId).offset().top;
        
        $("html, body").animate({
            scrollTop: elementOffset
        });
        
    })
    
});